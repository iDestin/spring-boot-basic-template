package com.qiang.todolist.bean;

import lombok.Builder;
import lombok.Data;

/**
 * @author qiang
 * @date 2020-12-04 01:04
 */
@Data
@Builder
public class UserToken {
    private String id;
    private String account;
    private String password;
}
