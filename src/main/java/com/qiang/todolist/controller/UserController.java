package com.qiang.todolist.controller;

import com.qiang.todolist.result.RequestFactor;
import com.qiang.todolist.result.Result;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * @author qiang
 * @date 2020-12-04 01:19
 */
@RestController
@RequestMapping(value = {"/users"})
public class UserController {

    @PostMapping(value = {"/hello"})
    public Result helloWorld(){
        Map<String,String> res = new HashMap<>();
        res.put("name","qiang");
        res.put("age","23");
        return RequestFactor.buildSuccessResult(res);
    }
}
