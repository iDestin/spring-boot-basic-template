package com.qiang.todolist.utils;

/**
 * @author qiang
 * @date 2020-12-04 01:02
 */

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.qiang.todolist.bean.UserToken;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class TokenUtil {
    /**
     * 过期时间一周
     */
    private static final long EXPIRE_TIME = 7*12*3600*1000;
    private static final String TOKEN_SECRET = "qcy999";
    /**
     * 生成签名，15分钟过期
     * @param **username**
     * @param **password**
     * @return
     */
    public static String sign(UserToken userToken, String type) {
        try {
            // 设置过期时间
            Date date = new Date(System.currentTimeMillis() + EXPIRE_TIME);
            // 私钥和加密算法
            Algorithm algorithm = Algorithm.HMAC256(TOKEN_SECRET);
            // 设置头部信息
            Map<String, Object> header = new HashMap<>(2);
            header.put("Type", "Jwt");
            header.put("alg", "HS256");
            String token = JWT.create()
                    .withHeader(header)
                    .withClaim("account", userToken.getAccount())
                    .withClaim("id", userToken.getId())
                    .withClaim("password", userToken.getPassword())
                    .withExpiresAt(date)
                    .sign(algorithm);

            return token;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 检验token是否正确
     * @param **token**
     * @return
     */
    //public static boolean verify(String token,String uri){
    //    String type = RedisUtil.get(token);
    //    if(ValidUtil.isEmpty(type)){
    //        return false;
    //    }
    //    if (!uri.startsWith(type)){
    //        return false;
    //    }
    //    try {
    //        Algorithm algorithm = Algorithm.HMAC256(TOKEN_SECRET);
    //        JWTVerifier verifier = JWT.require(algorithm).build();
    //        DecodedJWT jwt = verifier.verify(token);
    //        return true;
    //    } catch (Exception e){
    //        return false;
    //    }
    //}
}