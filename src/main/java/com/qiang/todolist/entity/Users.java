package com.qiang.todolist.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 用户实体类
 * @author qiang
 * @date 2020-12-04 00:44
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Users {
    private String id;
    private String account;
    private String userName;
    private String password;
    private Long createTime;
}
