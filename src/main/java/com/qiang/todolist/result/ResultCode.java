package com.qiang.todolist.result;

/**
 * @author qiang
 * @date 2020-12-04 01:08
 */
public enum  ResultCode {
    /**请求成功状态码*/
    SUCCESS(200),
    /**请求失败状态码*/
    FAIL(400),
    /**请求方法不支持*/
    UNAUTHORIZED(401),
    /**请求地址错误*/
    NOT_FOUND(404),
    /**服务端错误*/
    INTERNAL_SERVER_ERROR(500);

    public int code;

    ResultCode(int code) {
        this.code = code;
    }
}
