package com.qiang.todolist.result;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author qiang
 * @date 2020-12-04 01:08
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Result {
    private int code;
    private String message;
    private Object data;
    private final long timestamps = System.currentTimeMillis();
}
